﻿using Microsoft.AspNetCore.Authentication.MicrosoftAccount;
using Microsoft.Extensions.DependencyInjection;

namespace AD_umbraco
{
    public static class BackofficeAuthenticationExtensions
    {
        public static IUmbracoBuilder ConfigureAuthentication(this IUmbracoBuilder builder)
        {
            builder.AddBackOfficeExternalLogins(logins =>
            {
                const string schema = MicrosoftAccountDefaults.AuthenticationScheme;

                logins.AddBackOfficeLogin(
                    backOfficeAuthenticationBuilder =>
                    {
                        backOfficeAuthenticationBuilder.AddMicrosoftAccount(
                            // the scheme must be set with this method to work for the back office
                            backOfficeAuthenticationBuilder.SchemeForBackOffice(schema) ?? string.Empty,
                            options =>
                            {
                                //  by default this is '/signin-microsoft' but it needs to be changed to this
                                options.CallbackPath = "/umbraco/";
                                options.ClientId = "8528f0e0-cae0-46a5-b10f-3f4291b45630";
                                options.ClientSecret = "b4d5f30c-e75b-4d1f-a525-e076ce8bdd75";
                            });
                    });
            });
            return builder;
        }
    }
}